<?php

namespace Drupal\youtube_upload\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\youtube_upload\YoutubeUploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for Youtube upload routes.
 */
class YoutubeUploadController extends ControllerBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Youtube upload service.
   *
   * @var \Drupal\youtube_upload\YoutubeUploadService
   */
  protected $youtubeUploadService;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * YoutubeUploadController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   Request stack.
   * @param \Drupal\youtube_upload\YoutubeUploadService $youtubeUploadService
   *   YoutubeUploadService.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerChannelFactory
   *   LoggerFactory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              RequestStack $request,
                              YoutubeUploadService $youtubeUploadService,
                              MessengerInterface $messenger,
                              LoggerChannelFactory $loggerChannelFactory) {
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
    $this->youtubeUploadService = $youtubeUploadService;
    $this->messenger = $messenger;
    $this->logger = $loggerChannelFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack'),
      $container->get('youtube_upload.upload'),
      $container->get('messenger'),
      $container->get('logger.factory'),
    );
  }

  /**
   * OAuth2 Callback function for Google Authenticate.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect Response.
   */
  public function oAuth2Callback() {
    $code = $this->request->getCurrentRequest()->get('code');
    $error = $this->request->getCurrentRequest()->get('error');

    if ($code !== NULL  && $error === NULL) {
      $this->youtubeUploadService->authorize($code);
    }
    else {
      $this->messenger->addError($error);
      $this->logger->get('youtube_upload')->error($error);
    }
    return $this->redirect('youtube_upload.settings_form');
  }

  /**
   * Revoke Google Authentication.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect Response.
   */
  public function revoke() {
    $this->youtubeUploadService->revoke();
    return $this->redirect('youtube_upload.settings_form');
  }

}
