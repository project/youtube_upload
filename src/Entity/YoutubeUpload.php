<?php

namespace Drupal\youtube_upload\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Url;
use Drupal\youtube_upload\YoutubeUploadInterface;
use Drupal\user\UserInterface;

/**
 * Defines the youtube_upload entity class.
 *
 * @ContentEntityType(
 *   id = "youtube_upload",
 *   label = @Translation("Youtube upload"),
 *   label_collection = @Translation("Youtube uploads"),
 *   handlers = {
 *     "view_builder" = "Drupal\youtube_upload\YoutubeUploadViewBuilder",
 *     "list_builder" = "Drupal\youtube_upload\YoutubeUploadListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\youtube_upload\YoutubeUploadAccessControlHandler",
 *     "form" = {
 *       "delete" = "Drupal\youtube_upload\Form\DeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "youtube_upload",
 *   admin_permission = "administer youtube_upload",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/youtube-upload/{youtube_upload}",
 *     "delete-form" = "/admin/config/youtube-upload/{youtube_upload}/delete",
 *     "collection" = "/admin/config/youtube-upload/list"
 *   }
 * )
 */
class YoutubeUpload extends ContentEntityBase implements YoutubeUploadInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new youtube_upload entity is created, set the uid entity reference
   * to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->getTitle();
  }

  /**
   * {@inheritdoc}
   */
  public function getYoutubeId(): string {
    return $this->get('youtube_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setYoutubeId(string $id): YoutubeUploadInterface {
    $this->set('youtube_id', $id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): YoutubeUploadInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->get('description')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): YoutubeUploadInterface {
    $this->set('description', $description);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChannelId(): string {
    return $this->get('channel_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChannelId(string $channelId): YoutubeUploadInterface {
    $this->set('channel_id', $channelId);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChannelTitle(): string {
    return $this->get('channel_title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setChannelTitle(string $channelTitle): YoutubeUploadInterface {
    $this->set('channel_title', $channelTitle);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDuration(): int {
    return $this->get('duration')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setDuration(int $duration): YoutubeUploadInterface {
    $this->set('duration', $duration);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnailUrl(): string {
    if (!empty($this->getYoutubeId())) {
      return Url::fromUri('https://i.ytimg.com/vi/' . $this->getYoutubeId() . '/default.jpg')->toString();
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnailHeight(): int {
    return 90;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultThumbnailWidth(): int {
    return 120;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediumThumbnailUrl(): string {
    if (!empty($this->getYoutubeId())) {
      return Url::fromUri('https://i.ytimg.com/vi/' . $this->getYoutubeId() . '/mqdefault.jpg')->toString();
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getMediumThumbnailHeight(): int {
    return 180;
  }

  /**
   * {@inheritdoc}
   */
  public function getMediumThumbnailWidth(): int {
    return 320;
  }

  /**
   * {@inheritdoc}
   */
  public function getHighThumbnailUrl(): string {
    if (!empty($this->getYoutubeId())) {
      return Url::fromUri('https://i.ytimg.com/vi/' . $this->getYoutubeId() . '/hqdefault.jpg')->toString();
    }
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getHighThumbnailHeight(): int {
    return 360;
  }

  /**
   * {@inheritdoc}
   */
  public function getHighThumbnailWidth(): int {
    return 480;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * Get crated in date format.
   *
   * @return false|int
   *   Date.
   */
  public function getCreated() {
    return date('Y-m-d H:i:s', $this->getCreatedTime());
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): YoutubeUploadInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus(): string {
    return $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus(string $status): YoutubeUploadInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['youtube_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Youtube Id'))
      ->setDescription(t('Title.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 128)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('Title.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('The company website.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['channel_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Channel Id'))
      ->setDescription(t('Channel Id.'))
      ->setSetting('max_length', 128)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['channel_title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Channel Title'))
      ->setDescription(t('Channel Title.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duration'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Duration'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 6)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the youtube upload was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the youtube upload was last edited.'));

    return $fields;
  }

}
