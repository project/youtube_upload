<?php

namespace Drupal\youtube_upload\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\youtube_upload\YoutubeUploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for content type deletion.
 *
 * @internal
 */
class DeleteForm extends EntityConfirmFormBase {

  use EntityDeleteFormTrait {
    getQuestion as traitGetQuestion;
    logDeletionMessage as traitLogDeletionMessage;
    getDeletionMessage as traitGetDeletionMessage;
    getCancelUrl as traitGetCancelUrl;
  }

  /**
   * Youtube upload service.
   *
   * @var \Drupal\youtube_upload\YoutubeUploadService
   */
  protected $youtubeUploadService;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_upload_delete_form';
  }

  /**
   * DeleteForm constructor.
   *
   * @param \Drupal\youtube_upload\YoutubeUploadService $youtubeUploadService
   *   YoutubeUpload Service.
   */
  public function __construct(YoutubeUploadService $youtubeUploadService) {
    $this->youtubeUploadService = $youtubeUploadService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('youtube_upload.upload')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\youtube_upload\Entity\YoutubeUpload $entity  */
    $entity = $this->getEntity();

    $form['markup'] = [
      '#type' => 'markup',
      '#markup' => $this->t('It will be delete @video on Youtube too!<br />', ['@video' => $entity->getYoutubeId()]),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#button_type' => 'primary',
    ];

    $form['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#url' => $this->getCancelUrl(),
      '#attributes' => ['class' => ['button']],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\youtube_upload\Entity\YoutubeUpload $entity */
    $entity = $this->getEntity();
    $message = $this->getDeletionMessage();

    $response = $this->youtubeUploadService->delete($entity->getYoutubeId());
    $this->messenger()->addStatus($response);
    $this->logger('youtube')->error($response);
    $entity->delete();
    $form_state->setRedirectUrl($this->getRedirectUrl());

    $this->messenger()->addStatus($message);
    $this->logDeletionMessage();
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $this->getEntity();
    return $entity->isDefaultTranslation() ? $this->traitGetCancelUrl() : $entity->toUrl('canonical');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->traitGetQuestion();
  }

}
