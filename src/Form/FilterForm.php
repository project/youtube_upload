<?php

namespace Drupal\youtube_upload\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\TempStoreException;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Youtube upload filter form.
 */
class FilterForm extends FormBase {

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Private temp store.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_upload_filter_form';
  }

  /**
   * FilterForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   RequestStack.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store
   *   Private temp store.
   */
  public function __construct(RequestStack $request, PrivateTempStoreFactory $temp_store) {
    $this->request = $request;
    $this->tempStore = $temp_store->get('youtube_upload');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('tempstore.private'),
    );
  }

  /**
   * Build form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Render array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $youtube_id = $this->request->getCurrentRequest()->get('youtube_id');
    $title = $this->request->getCurrentRequest()->get('title');
    $channel_title = $this->request->getCurrentRequest()->get('channel_title');
    $created = $this->request->getCurrentRequest()->get('created');

    try {
      $this->tempStore->set('youtube_id', $youtube_id);
      $this->tempStore->set('title', $title);
      $this->tempStore->set('channel_title', $channel_title);
      $this->tempStore->set('created', $created);
    }
    catch (TempStoreException $e) {
      watchdog_exception('youtube_upload', $e);
    }

    $form['authorized'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<a class="button button--primary" href=":url">Upload new video</a><br />', [':url' => '/admin/config/youtube-upload/upload']),
    ];

    $form['wrapper'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'class' => [
          'form--inline',
          'clearfix',
        ],
      ],
    ];

    $form['wrapper']['youtube_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Youtube Id'),
      '#maxlength' => 64,
      '#size' => 25,
      '#default_value' => $youtube_id ?? '',
    ];

    $form['wrapper']['title'] = [
      '#type' => 'textfield',
      '#maxlength' => 64,
      '#size' => 25,
      '#title' => $this->t('Title'),
      '#default_value' => $title ?? '',
    ];

    $form['wrapper']['channel_title'] = [
      '#type' => 'textfield',
      '#maxlength' => 64,
      '#size' => 25,
      '#title' => $this->t('Channel title'),
      '#default_value' => $channel_title ?? '',
    ];

    $form['wrapper']['created'] = [
      '#type' => 'date',
      '#title' => $this->t('Created'),
      '#default_value' => $created ?? '',
    ];

    $form['wrapper']['actions'] = [
      '#type' => 'container',
      '#tree' => TRUE,
      '#attributes' => [
        'class' => [
          'form-actions',
        ],
      ],
    ];

    $form['wrapper']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    if (!empty($youtube_id) ||
      !empty($title) ||
      !empty($channel_title) ||
      !empty($created)) {
      $form['wrapper']['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
        '#validate' => [],
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $youtube_id = $form_state->getValue('wrapper')['youtube_id'] ?? '';
    $title = $form_state->getValue('wrapper')['title'] ?? '';
    $channel_title = $form_state->getValue('wrapper')['channel_title'] ?? '';
    $created = $form_state->getValue('wrapper')['created'] ?? '';

    try {
      $this->tempStore->set('youtube_id', $youtube_id);
      $this->tempStore->set('title', $title);
      $this->tempStore->set('channel_title', $channel_title);
      $this->tempStore->set('created', $created);
    }
    catch (TempStoreException $e) {
      watchdog_exception('youtube_upload', $e);
    }

    $url = Url::fromRoute('youtube_upload.list')
      ->setRouteParameters([
        'youtube_id' => $youtube_id,
        'title' => $title,
        'channel_title' => $channel_title,
        'created' => $created,
      ]);
    $form_state->setRedirectUrl($url);

  }

  /**
   * Reset filter form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function resetForm(array &$form, FormStateInterface $form_state) {
    try {
      $this->tempStore->set('youtube_id', '');
      $this->tempStore->set('title', '');
      $this->tempStore->set('channel_title', '');
      $this->tempStore->set('created', '');
    }
    catch (TempStoreException $e) {
      watchdog_exception('youtube_upload', $e);
    }

    $url = Url::fromRoute('youtube_upload.list');
    $form_state->setRedirectUrl($url);
  }

}
