<?php

namespace Drupal\youtube_upload\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\youtube_upload\YoutubeUploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Youtube upload settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'youtube_upload.settings';

  /**
   * Youtube upload service.
   *
   * @var \Drupal\youtube_upload\YoutubeUploadService
   */
  protected $youtubeUploadService;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_upload_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS];
  }

  /**
   * SettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   ConfigFactory.
   * @param \Drupal\youtube_upload\YoutubeUploadService $youtubeUploadService
   *   YoutubeUploadService.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    YoutubeUploadService $youtubeUploadService) {
    $this->youtubeUploadService = $youtubeUploadService;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('youtube_upload.upload')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $isAuthorized = !empty($this->config(self::SETTINGS)->get('access_token'));

    $hasCredentials = !empty($this->config(self::SETTINGS)->get('client_id')) &&
      !empty($this->config(self::SETTINGS)->get('client_secret')) &&
      !empty($this->config(self::SETTINGS)->get('redirect_uri'));

    if ($isAuthorized) {
      $form['authorized'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<a class="button" href=":url">Revoke Google Authorization</a><br />', [':url' => '/admin/config/youtube-upload/revoke']),
      ];
    }
    else if ($hasCredentials) {
      $form['unauthorized'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<a class="button" href=":url">Authorize</a><br />', [':url' => $this->youtubeUploadService->getAuthUrl()]),
      ];
    }

    $form['desc'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Google API registration process: <a href=":url" target="_blank">:url</a>', [':url' => 'https://www.drupal.org/node/2636562']),
    ];

    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#required' => TRUE,
      '#default_value' => $this->config(self::SETTINGS)->get('client_id'),
      '#description' => $this->t('Client ID from Google Console API credential'),
      '#attributes' => [
        'disabled' => $isAuthorized,
      ],
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#required' => TRUE,
      '#default_value' => $this->config(self::SETTINGS)->get('client_secret'),
      '#description' => $this->t('Client Secret from Google Console API credential'),
      '#attributes' => [
        'disabled' => $isAuthorized,
      ],
    ];

    $callbackUrl = Url::fromRoute('youtube_upload.oauth2callback', [], ['absolute' => TRUE])->toString();

    $form['redirect_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Redirect uri'),
      '#description' => $this->t('Fill field and Google Console redirect uri with %web', ['%web' => $callbackUrl]),
      '#required' => TRUE,
      '#default_value' => $this->config(self::SETTINGS)->get('redirect_uri'),
      '#attributes' => [
        'disabled' => $isAuthorized,
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#button_type' => 'primary',
      '#attributes' => [
        'disabled' => $isAuthorized,
      ],
    ];

    return $form + parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::SETTINGS)
      ->set('client_id', $form_state->getValue('client_id'))
      ->set('client_secret', $form_state->getValue('client_secret'))
      ->set('redirect_uri', $form_state->getValue('redirect_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
