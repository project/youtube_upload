<?php

namespace Drupal\youtube_upload\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\youtube_upload\YoutubeUploadService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Youtube upload filter form.
 */
class UploadForm extends FormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Youtube upload service.
   *
   * @var \Drupal\youtube_upload\YoutubeUploadService
   */
  protected $youtubeUploadService;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'youtube_upload_upload_form';
  }

  /**
   * UploadForm constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\youtube_upload\YoutubeUploadService $youtubeUploadService
   *   YoutubeUploadService.
   */
  public function __construct(MessengerInterface $messenger, YoutubeUploadService $youtubeUploadService) {
    $this->messenger = $messenger;
    $this->youtubeUploadService = $youtubeUploadService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('youtube_upload.upload')
    );
  }

  /**
   * Build form.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Render array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $form['#attributes'] = [
      'enctype' => 'multipart/form-data',
    ];

    $form['authorized'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Lorem ipsum dolor sit'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
    ];

    $form['tags'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('Comma separated list'),
    ];

    $validators = [
      'file_validate_extensions' => [
        'mov mpeg4 mp4 avi wmv mpegps flv',
      ],
    ];

    $form['video'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('File'),
      '#description' => $this->t('Allowed formats: %formats', ['%formats' => $validators['file_validate_extensions'][0]]),
      '#upload_location' => 'public://youtube_upload',
      '#upload_validators' => $validators,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Upload'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $file = $form_state->getValue('video');
    $title = $form_state->getValue('title');
    $description = $form_state->getValue('description');
    $tags = $form_state->getValue('tags');
    if (!empty($tags)) {
      $tags = explode(',', $tags);
      $tags = array_map(static function ($item) {
        return trim($item);
      }, $tags);
    }
    else {
      $tags = [];
    }

    $success = $this->youtubeUploadService->upload($title, $description, $file[0], $tags);

    if ($success === TRUE) {
      $url = Url::fromRoute('youtube_upload.list');
      $form_state->setRedirectUrl($url);
    }
    else {
      $this->messenger->addError($success);
    }
  }

}
