<?php

namespace Drupal\youtube_upload;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the youtube_upload entity type.
 */
class YoutubeUploadAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account,
          'view youtube_upload');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account,
          ['edit youtube_upload', 'administer youtube_upload'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account,
          ['delete youtube_upload', 'administer youtube_upload'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account,
      ['create youtube_upload', 'administer youtube_upload'], 'OR');
  }

}
