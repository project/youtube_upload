<?php

namespace Drupal\youtube_upload;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a youtube_upload entity type.
 */
interface YoutubeUploadInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the Youtube Id.
   *
   * @return string
   *   Youtube ID.
   */
  public function getYoutubeId(): string;

  /**
   * Sets the Youtube Id.
   *
   * @param string $id
   *   The Youtube Id.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setYoutubeId(string $id): YoutubeUploadInterface;

  /**
   * Gets the Title.
   *
   * @return string
   *   Title.
   */
  public function getTitle(): string;

  /**
   * Sets the Title.
   *
   * @param string $title
   *   The Title.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setTitle(string $title): YoutubeUploadInterface;

  /**
   * Gets Description.
   *
   * @return string
   *   Description.
   */
  public function getDescription(): string;

  /**
   * Sets Description.
   *
   * @param string $description
   *   Description.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setDescription(string $description): YoutubeUploadInterface;

  /**
   * Gets Channel Id.
   *
   * @return string
   *   Channel Id.
   */
  public function getChannelId(): string;

  /**
   * Sets Channel Id.
   *
   * @param string $channelId
   *   Channel Id.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setChannelId(string $channelId): YoutubeUploadInterface;

  /**
   * Gets Channel Title.
   *
   * @return string
   *   Channel Title.
   */
  public function getChannelTitle(): string;

  /**
   * Sets Channel Title.
   *
   * @param string $channelTitle
   *   Channel Title.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setChannelTitle(string $channelTitle): YoutubeUploadInterface;

  /**
   * Gets Duration.
   *
   * @return int
   *   Duration in seconds.
   */
  public function getDuration(): int;

  /**
   * Sets Duration.
   *
   * @param int $duration
   *   Duration in seconds.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setDuration(int $duration): YoutubeUploadInterface;

  /**
   * Gets Default Thumbnail Url.
   *
   * @return string
   *   DefaultThumbnailUrl.
   */
  public function getDefaultThumbnailUrl(): string;

  /**
   * Gets Default Thumbnail Height.
   *
   * @return int
   *   Default Thumbnail Height.
   */
  public function getDefaultThumbnailHeight(): int;

  /**
   * Gets Default Thumbnail Width.
   *
   * @return int
   *   Default Thumbnail Width.
   */
  public function getDefaultThumbnailWidth(): int;

  /**
   * Gets Medium Thumbnail Url.
   *
   * @return string
   *   MediumThumbnailUrl.
   */
  public function getMediumThumbnailUrl(): string;

  /**
   * Gets Medium Thumbnail Height.
   *
   * @return int
   *   Medium Thumbnail Height.
   */
  public function getMediumThumbnailHeight(): int;

  /**
   * Gets Medium Thumbnail Width.
   *
   * @return int
   *   Medium Thumbnail Width.
   */
  public function getMediumThumbnailWidth(): int;

  /**
   * Gets High Thumbnail Url.
   *
   * @return string
   *   HighThumbnailUrl.
   */
  public function getHighThumbnailUrl(): string;

  /**
   * Gets High Thumbnail Height.
   *
   * @return int
   *   High Thumbnail Height.
   */
  public function getHighThumbnailHeight(): int;

  /**
   * Gets High Thumbnail Width.
   *
   * @return int
   *   High Thumbnail Width.
   */
  public function getHighThumbnailWidth(): int;

  /**
   * Gets the youtube_upload creation timestamp.
   *
   * @return int
   *   Creation timestamp of the youtube_upload.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the youtube_upload creation timestamp.
   *
   * @param int $timestamp
   *   The youtube_upload creation timestamp.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setCreatedTime(int $timestamp): YoutubeUploadInterface;

  /**
   * Returns the youtube_upload status.
   *
   * @return string
   *   TRUE if the youtube_upload is enabled, FALSE otherwise.
   */
  public function getStatus(): string;

  /**
   * Sets the youtube_upload status.
   *
   * @param string $status
   *   TRUE to enable this youtube_upload, FALSE to disable.
   *
   * @return \Drupal\youtube_upload\YoutubeUploadInterface
   *   The called youtube_upload entity.
   */
  public function setStatus(string $status): YoutubeUploadInterface;

}
