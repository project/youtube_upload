<?php

namespace Drupal\youtube_upload;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Drupal\youtube_upload\Form\FilterForm;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a list controller for the youtube_upload entity type.
 */
class YoutubeUploadListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $request;

  /**
   * Count of filtered entities.
   *
   * @var int
   *   Count of entities.
   */
  protected $count;

  /**
   * Constructs a new YoutubeUploadListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The request stack.
   */
  public function __construct(EntityTypeInterface $entity_type,
                              EntityStorageInterface $storage,
                              DateFormatterInterface $date_formatter,
                              RedirectDestinationInterface $redirect_destination,
                              FormBuilderInterface $form_builder,
                              EntityTypeManagerInterface $entity_type_manager,
                              RequestStack $request
  ) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->formBuilder = $form_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('form_builder'),
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function load() {
    $entity_query = $this->entityTypeManager->getStorage('youtube_upload')->getQuery();

    $query_string = $this->request->getCurrentRequest()->getQueryString();
    if (!empty($query_string)) {
      parse_str($query_string, $query_string_array);
    }

    $header = $this->buildHeader();

    $entity_query->pager(10);
    $entity_query->tableSort($header);

    if (!empty($query_string_array['youtube_id'])) {
      $entity_query->condition('youtube_id', $query_string_array['youtube_id']);
    }

    if (!empty($query_string_array['title'])) {
      $entity_query->condition('title', '%' . $query_string_array['title'] . '%', 'LIKE');
    }

    if (!empty($query_string_array['channel_title'])) {
      $entity_query->condition('channel_title', '%' . $query_string_array['channel_title'] . '%', 'LIKE');
    }

    if (!empty($query_string_array['created'])) {
      $date = strtotime($query_string_array['created'] . ' 00:00:00');
      $entity_query->condition('created', $date, '>=');
    }

    $ids = $entity_query->execute();

    $this->count = count($ids);

    return $this->storage->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['form'] = $this->formBuilder->getForm(FilterForm::class);

    $build['table'] = parent::render();

    $total = $this->getStorage()
      ->getQuery()
      ->count()
      ->execute();

    $build['summary']['#markup'] = $this->t('Total uploads: @total, filtered: @filtered',
      ['@total' => $total, '@filtered' => $this->count]);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {

    $header['youtube_id'] = [
      'data' => $this->t('Youtube ID'),
      'field' => 'youtube_id',
      'specifier' => 'youtube_id',
    ];

    $header['thumbnail'] = $this->t('Thumbnail');

    $header['title'] = [
      'data' => $this->t('Title'),
      'field' => 'title',
      'specifier' => 'title',
    ];
    $header['channel_title'] = [
      'data' => $this->t('Channel title'),
      'field' => 'channel_title',
      'specifier' => 'channel_title',
    ];

    $header['created'] = [
      'data' => $this->t('Created'),
      'field' => 'created',
      'specifier' => 'created',
    ];

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\youtube_upload\YoutubeUploadInterface $entity */
    $url = Url::fromUri('https://www.youtube.com/watch?v=' . $entity->getYoutubeId(), ['attributes' => ['target' => '_blank']]);
    $link = Link::fromTextAndUrl($entity->getYoutubeId(), $url);
    $row['youtube_id'] = $link;
    $row['thumbnail'] = Markup::create('<img src="' . $entity->getDefaultThumbnailUrl() . '" >');
    $row['title'] = $entity->getTitle();
    $row['channel_title'] = $entity->getChannelTitle();
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime(), 'custom', 'D, d/m/y g:i A');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();

    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }

    return $operations;
  }

}
