<?php

namespace Drupal\youtube_upload;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\youtube_upload\Form\SettingsForm;
use Google\Service\YouTube;
use Google\Service\YouTube\Video;
use Google\Service\YouTube\VideoSnippet;
use Google\Service\YouTube\VideoStatus;
use GuzzleHttp\Psr7\Response;

/**
 * Upload service for youtube function.
 */
class YoutubeUploadService {

  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $logger;

  /**
   * Google client.
   *
   * @var \Google\Client
   */
  protected $googleClient;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * YoutubeUploadService constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config Factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   EntityType manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger Factory.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   File system.
   */
  public function __construct(MessengerInterface $messenger,
                              ConfigFactoryInterface $config_factory,
                              EntityTypeManagerInterface $entity_type_manager,
                              LoggerChannelFactoryInterface $loggerChannelFactory,
                              FileSystemInterface $fileSystem
  ) {
    $this->messenger = $messenger;
    $this->config = $config_factory->getEditable(SettingsForm::SETTINGS);
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $loggerChannelFactory;
    $this->fileSystem = $fileSystem;

    $this->googleClient = new \Google_Client();
    $this->googleClient->setScopes([
      YouTube::YOUTUBE,
      YouTube::YOUTUBE_UPLOAD,
    ]);
    $this->googleClient->setClientId($this->config->get('client_id'));
    $this->googleClient->setClientSecret($this->config->get('client_secret'));
    $this->googleClient->setRedirectUri($this->config->get('redirect_uri'));
    $this->googleClient->setAccessType('offline');
    $this->googleClient->setApprovalPrompt('force');

  }

  /**
   * Get Google Authentication Url.
   *
   * @return string
   *   Authentication Url.
   */
  public function getAuthUrl(): string {
    return $this->googleClient->createAuthUrl();
  }

  /**
   * Authorize with code form OAuth2 callback.
   *
   * @param string $code
   *   Authorization code from OAuht2 callback.
   */
  public function authorize(string $code) {

    $response = $this->googleClient->fetchAccessTokenWithAuthCode($code);
    $access_token = $this->googleClient->getAccessToken();
    $refresh_token = $this->googleClient->getRefreshToken();

    if ($access_token !== NULL && $refresh_token !== NULL) {
      $this->config
        ->set('access_token', $access_token)
        ->set('refresh_token', $refresh_token)
        ->save();
      $this->messenger->addMessage('Token granted');
      $this->logger->get('youtube_upload')->notice('Token granted!');
    }
    else {
      $this->messenger->addError($response['error_description']);
      $this->logger->get('youtube_upload')->error($response['error_description']);
    }
  }

  /**
   * Revoke authorization.
   */
  public function revoke() {
    $this->googleClient->revokeToken();
    $this->config->set('access_token', '')->save();
    $this->config->set('refresh_token', '')->save();
  }

  /**
   * Refresh Access Token, get new token with refresh token.
   */
  public function refreshAccessToken() {
    $this->googleClient->setAccessToken($this->config->get('access_token'));
    if ($this->googleClient->isAccessTokenExpired()) {
      $this->googleClient->refreshToken($this->config->get('refresh_token'));
      $access_token = $this->googleClient->getAccessToken();
      $this->config
        ->set('access_token', $access_token)
        ->save();
    }
  }

  /**
   * Upload video to youtube, save data to youtube_upload entity.
   *
   * @param string $title
   *   Video title.
   * @param string $description
   *   Video description.
   * @param string $fileId
   *   File id.
   * @param array $tags
   *   Tags in array.
   *
   * @return bool|mixed
   *   Message.
   */
  public function upload(string $title, string $description, string $fileId, array $tags = []) {

    $this->refreshAccessToken();
    $youtube = new YouTube($this->googleClient);
    $video = new Video();
    $videoSnippet = new VideoSnippet();
    $videoSnippet->setTitle($title);
    $videoSnippet->setDescription($description);
    if (!empty($tags)) {
      $videoSnippet->setTags($tags);
    }

    $video->setSnippet($videoSnippet);

    $videoStatus = new VideoStatus();
    $videoStatus->setPrivacyStatus('public');
    $video->setStatus($videoStatus);

    $file = $this->entityTypeManager->getStorage('file')->load($fileId);
    $path = $this->fileSystem->realpath($file->getFileUri());

    try {
      $response = $youtube->videos->insert(
        'snippet,status',
        $video,
        [
          'data' => file_get_contents($path),
          'mimeType' => 'video/*',
          'uploadType' => 'multipart',
        ]
      );

      $values = [
        'youtube_id' => $response->id,
        'title' => $title,
        'description' => $description,
        'channel_id' => $response['snippet']['channelId'],
        'channel_title' => $response['snippet']['channelTitle'],
      ];

      try {
        $entity = $this->entityTypeManager->getStorage('youtube_upload')
          ->create($values);
        $entity->save();
      }
      catch (EntityStorageException $e) {
        $this->logger->get('youtube_upload')->error($e->getMessage());
      }

      $file->delete();
      return TRUE;
    }
    catch (\Exception $e) {
      $file->delete();
      $this->logger->get('youtube_upload')->error($e->getMessage());
      $error = json_decode($e->getMessage(), TRUE);
      return $error['error']['message'];
    }

  }

  /**
   * Delete video on youtube.
   *
   * @param string $youtube_id
   *   Youtube id.
   *
   * @return \GuzzleHttp\Psr7\Request|mixed
   *   Message.
   */
  public function delete(string $youtube_id) {
    $this->refreshAccessToken();
    $youtube = new YouTube($this->googleClient);
    try {
      $response = $youtube->videos->delete($youtube_id);
      if ($response instanceof Response) {
        return $response->getStatusCode();
      }

      $error = json_decode($response, TRUE);
      return $error['error']['message'];

    }
    catch (\Exception $e) {
      $this->logger->get('youtube_upload')->error($e->getMessage());
      $error = json_decode($e->getMessage(), TRUE);
      return $error['error']['message'];
    }
  }

}
